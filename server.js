const express = require("express");
let app = express();

const port = 8080;

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/parser", (req, res) => {
  res.status(200);
});

app.listen(port, () => {
  console.log(`Server up and run om port ${port} `);
});
