const cheerio = require("cheerio");
const fs = require("fs");
const parser = require("./parser");

const listPages = {
  machine: "./pages/mashin.html",
  computers: "./pages/computers.html"
};

fs.readFile(listPages.computers, "utf8", (error, data) => {
  if (error) throw error;
  console.log("--- LOG - Read file...");
  let arr = parser.parser(data);
  let myJsonString = JSON.stringify(arr);

  fs.writeFile(`./json/1.json`, myJsonString, err => {
    if (err) {
      return console.log(err);
    }
    console.log("--- LOG - The file was saved! ");
  });
});
