const cheerio = require("cheerio");

const parser = data => {
  console.log("--- LOG - Start parse file...");
  let arr = [];

  let $ = cheerio.load(data),
    filters = $(
      ".n-filter-panel-extend.i-bem.n-filter-panel-extend_js_inited"
    ).html();

  let $blocks = cheerio.load(filters),
    activeBlock = $blocks(
      ".n-filter-block.n-filter-cookie.i-bem.n-filter-block_type_normal.b-zone.b-spy-visible.n-filter-block_closed_no.n-filter-block_pos_aside.n-filter-block_js_inited.n-filter-cookie_js_inited"
    ).each((i, elm) => {
      let Bar = {};
      let zero = [];
      let bar = $blocks(elm).html();
      let foo = cheerio.load(bar),
        title = foo(".title").text(),
        category = foo(".n-filter-block__list-items.i-bem").each((i, elem2) => {
          let f = foo(elem2)
            .find(".checkbox__label")
            .each((i, elem) => {
              zero.push(foo(elem).text());
            });
        });
      Bar[title] = zero;
      arr.push(Bar);
      console.log("--- LOG - Active Block Ready");
    }),
    blocks = $blocks(
      ".n-filter-block.n-filter-cookie.i-bem.n-filter-block_type_normal.b-zone.b-spy-visible.n-filter-block_closed_yes.n-filter-block_pos_aside.n-filter-block_js_inited.n-filter-cookie_js_inited"
    ).each((i, elm) => {
      let Bar = {};
      let cat1 = [],
        cat2 = [],
        cat3 = [];

      let bar = $blocks(elm).html();
      let foo = cheerio.load(bar),
        title = foo(".title").text();

      let category1 = foo(
        ".n-filter-block__list-items.n-filter-block__list-items_scroll_yes.i-bem"
      ).each((i, elem) => {
        let title = foo(elem)
          .find(
            ".n-filter-block__list_title.i-bem.n-filter-block__item_js_inited"
          )
          .each((i, elem) => {
            cat1.push(foo(elem).text());
          });
        let f = foo(elem)
          .find(".checkbox.checkbox_size_s.i-bem.checkbox_theme_normal")
          .each((i, elem) => {
            cat1.push(foo(elem).text());
          });

          console.log("--- LOG - Category 1 parse ...");
      });

      let category2 = foo(
        ".n-filter-block__body.i-bem.n-filter-block__body_viewType_list.n-filter-block__body_js_inited"
      ).each((i, elem2) => {
        let f = foo(elem2)
          .find(".checkbox__label")
          .each((i, elem) => {
            cat2.push(foo(elem).text());
          });

          console.log("--- LOG - Category 2 parse ...");
      });

      let category3 = foo(
        ".n-filter-block__body.i-bem.n-filter-block__body_viewType_boolean.n-filter-block__body_js_inited"
      ).each((i, elem3) => {
        let f = foo(elem3)
          .find(
            ".radiobox__radio.i-bem.n-filter-block__item.n-filter-block__item_js_inited"
          )
          .each((i, elem) => {
            cat3.push(foo(elem).text());
          });

          console.log("--- LOG - Category 3 parse ...");
      });

      if (category1) {
        Bar[title] = cat1;
      }
      if (category2.text()) {
        Bar[title] = cat2;
      }
      if (category3.text()) {
        Bar[title] = cat3;
      }

      arr.push(Bar);
        console.log("--- LOG - Parsing finished ...");
    });

  return arr;
};

module.exports.parser = parser;
